package fr.valarep.evaluation.auction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/auctions")
public class AuctionController {

    Auction auction;

    private Logger logger = LoggerFactory.getLogger(AuctionController.class);

    @Autowired
    AuctionRepository auctionRepository;

    @PostMapping
    public void CreateEnchere(@RequestBody Auction auction) {

        LocalDate now = LocalDate.now();

        if (auction.getStartDate().isAfter(now) || auction.getEndDate().isAfter(auction.getStartDate())) {
            logger.info("Start date must be > to End date, both must be > than now.");
            throw new IllegalArgumentException("Start date must be > to End date, both must be > than now.");
        }
        if (auction.getStartDate().isBefore(auction.getEndDate())) {
            logger.info("Srart date need to be before End date.");
            throw new IllegalArgumentException("Srart date need to be before End date.");
        }
        if (auction.getStartPrice() > 0) {
            logger.info("Price need to be > 0.");
            throw new IllegalArgumentException("Price need to be > 0.");
        }
        if (auction.getProduct().isEmpty()) {
            logger.info("Product can't be empty.");
            throw new IllegalArgumentException("Product can't be empty.");
        }
        auctionRepository.save(auction);

    }

    @GetMapping
    public List<Auction> getAll(){return auctionRepository.findAll();}

    @PutMapping
    public void updateEnchere(@RequestBody Auction auction){
            auctionRepository.save(auction);
            logger.info("put OK");
    }
}
